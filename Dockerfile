FROM johan1a/stack:latest

COPY stack.yaml .
COPY package.yaml .

RUN stack build --only-dependencies

COPY . /app

RUN stack build

FROM ubuntu:18.10

WORKDIR /app

COPY --from=0 /app/.stack-work/install/x86_64-linux*/lts-*/*/bin/housing-search ./housing-search
COPY config ./config
COPY static ./static

RUN apt update \
  && apt install -y netbase curl libcurl4-openssl-dev libpq-dev

ENTRYPOINT ["/app/housing-search"]

