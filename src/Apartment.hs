{-# LANGUAGE OverloadedStrings, UnicodeSyntax #-}
{-# LANGUAGE DeriveGeneric #-}

module Apartment  where
import Data.CSV
import GHC.Generics (Generic)
import Data.Csv

import Data.Aeson (FromJSON, ToJSON)

type Address = String
type Area = String
type Municipality = String
type Rooms = Int
type SquareArea = String
type TotalRent = String

data Apartment = Apartment
    { self :: !String
    , address :: !String
    , area :: !String
    , municipality :: !String
    , rooms :: !Int
    , squareArea :: !String
    , totalRent :: !String
    , hostName :: !String
    , objInfoList :: !String
    , availableDate :: !String
    , commuteDistance :: !String
    , commuteFrequency :: !String
    , commuteDuration :: !String
    }
    deriving (Show, Eq, Generic)

apartmentDefault = Apartment "" "" "" "" 0 "" "" "" "" "" "" "" ""

instance FromNamedRecord Apartment where
    parseNamedRecord r = Apartment <$> r .: "self"
                                    <*> r .: "address"
                                    <*> r .: "area"
                                    <*> r .: "municipality"
                                    <*> r .: "rooms"
                                    <*> r .: "squareArea"
                                    <*> r .: "totalRent"
                                    <*> r .: "hostName"
                                    <*> r .: "objInfoList"
                                    <*> r .: "availableDate"
                                    <*> r .: "commuteDistance"
                                    <*> r .: "commuteFrequency"
                                    <*> r .: "commuteDuration"



instance FromJSON Apartment
instance ToJSON Apartment
