{-# LANGUAGE OverloadedStrings, UnicodeSyntax #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Lib
    ( getHtml,
      getApartments,
      getNew,
      handleNew,
      dataRetrievalJob
    ) where
import Text.HTML.Scalpel
import Network.Wreq
import Control.Lens
import Data.Char (isSpace)
import Data.Text.Lazy.Encoding
import Data.Text.Lazy hiding(map)
import Control.Exception (try, finally, catch, Exception (..), IOException)
import System.IO.Strict as S
import Data.Text (Text)
import Data.CSV
import Data.Csv
import Data.List as L hiding(map)
import qualified Data.Vector as V
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString as BS
import Debug.Trace
import Data.Config
import Data.Maybe
import Slack
import Distance
import Apartment
import System.Log.Logger
import System.Log.Handler.Simple (fileHandler, streamHandler, GenericHandler)
import System.Log.Handler (setFormatter)
import System.Log.Formatter
import System.IO (stderr, stdout, Handle)
import Prelude (read, writeFile)

import Import hiding(say, (++), responseBody, decodeUtf8, unpack, traceShow, try, sortBy)

import Model

import Settings

import Data.Pool (Pool)
import Database.Persist.Postgresql (withPostgresqlPool, runSqlPersistMPool)
import Control.Monad.Logger (runStdoutLoggingT)

libLogger = "dataretrieval"

initLogging :: IO ()
initLogging = do
    updateGlobalLogger rootLoggerName removeHandler
    streamHandler <- streamHandler stdout INFO
    stdOutHandler <- return $
            setFormatter streamHandler (simpleLogFormatter "[$time : $loggername] $msg")
    updateGlobalLogger libLogger (setLevel INFO . setHandlers [stdOutHandler])
    --updateGlobalLogger libLogger $ setLevel INFO

dataRetrievalJob :: AppSettings -> IO ()
dataRetrievalJob conf = do
  initLogging
  infoM libLogger "dataRetrievalJob starting"
  infoM libLogger "Retrieving HTML"
  html <- liftIO $ getHtml
  infoM libLogger "Parsing HTML"
  let allNew = case getApartments html of
                          Just val -> val
                          Nothing -> []
  infoM libLogger $ "Nbr apartments found from query: " ++ (show $ L.length allNew)
  runStdoutLoggingT $ withPostgresqlPool (dbUrl conf) 10 $ \pool  -> liftIO $ do
    flip runSqlPersistMPool (pool :: (Pool SqlBackend)) $! do
      liftIO $ infoM libLogger "Comparing to existing aparments in DB"
      allExistingDB <- selectList [] []
      let existing = map fromDbApartment allExistingDB
      new <- liftIO $ getDistances conf $ getNew existing allNew []

      liftIO $ infoM libLogger "Saving new apartments to DB"
      forM new (\a -> insertEntity (toDbApartment a))
      liftIO $ liftIO $ handleNew conf new

      return ()

  infoM libLogger "dataRetrievalJob finished"

withFormatter :: GenericHandler Handle -> GenericHandler Handle
withFormatter handler = setFormatter handler formatter
    -- http://hackage.haskell.org/packages/archive/hslogger/1.1.4/doc/html/System-Log-Formatter.html
    where formatter = simpleLogFormatter "[$time $loggername $prio] $msg"

currentFileName = "apartments.csv"

saveData :: [Apartment] -> IO ()
saveData aa = Prelude.writeFile currentFileName $ makeCsv $ sortApartments aa

sortApartments :: [Apartment] -> [Apartment]
sortApartments aa = sortBy compareApartment aa

handleNew config new
  | nbrNew > 0 = do
      say csvMsg config
      say foundMsg config
      infoM libLogger foundMsg
      infoM libLogger "Posted to Slack."
  | otherwise = infoM libLogger foundMsg
    where nbrNew = (L.length new)
          foundMsg = "Found " ++ (show nbrNew) ++ " new apartments"
          csvMsg = ("```" ++ (makeCsv $ new) ++ "```")

getNew :: [Apartment] -> [Apartment] -> [Apartment] -> [Apartment]
getNew [] [] acc = acc
getNew old [] acc = acc
getNew [] nn acc = nn
getNew old (n:nn) acc =
  case (L.find (\x -> sameApartment n x) old) of
      Just a -> getNew old nn acc
      Nothing -> getNew old nn (n : acc)

compareApartment a b = compare (self a) (self b)

sameApartment :: Apartment -> Apartment -> Bool
sameApartment a b = (self a) == (self b)


getPrevData :: IO [Apartment]
getPrevData = do
  result <- try (BS.readFile currentFileName) :: IO (Either IOException BS.ByteString)
  case result of
    Left ex  -> return []
    Right val -> case (decodeByName (BL.fromStrict val)) of
                    Left err -> return $ traceShow err []
                    Right (_, v) -> return $ V.toList v

getHtml = do
    r <- getWith opts "https://www.boplatssyd.se/lagenheter"
    return $ unpack $ decodeUtf8 (r ^. responseBody)
        where opts = defaults & param "location[cities][]" .~ ["11"]
                        & param "types[L]" .~ ["L"]
                        & param "types[T]" .~ ["T"]
                        & param "rooms[min]" .~ ["1"]
                        & param "rooms[max]" .~ ["s10"]
                        & param "size[min]" .~ ["0"]
                        & param "size[max]" .~ ["350"]
                        & param "rent[max]" .~ ["10000"]
                        & param "view_mode" .~ ["grid"]
                        & param "Content-Type: text/html; charset" .~ ["utf-8"]

getApartments:: String -> Maybe [Apartment]
getApartments html = scrapeStringLike html apartments
   where
       apartments :: Scraper String [Apartment]
       apartments = chroots ("article" @: [hasClass "object-teaser"]) apartment

       apartment :: Scraper String Apartment
       apartment = do
           self <- attr "href" $ "a" @: [hasClass "apartment"]
           address <- text $ "span" @: [hasClass "address"]
           area <- text $ "span" @: [hasClass "area"]
           municipality <- text $ "span" @: [hasClass "municipality"]
           rooms <- attr "data-apartment-rooms" $ "li" @: [hasClass "size-designation"]
           squareArea <- attr "data-apartment-size" $ "li" @: [hasClass "size-kvm"]
           totalRent <- attr "data-apartment-rent" $ "li" @: [hasClass "total-rent"]
           hostName <- attr "data-apartment-landlord" $ "li" @: [hasClass "host-name"]
           objInfoList <- objectInfoList
           availableDate <- attr "data-apartment-available" $ "li" @: [hasClass "available-date"]
           return $ apartmentDefault {
             self = ("https://www.boplatssyd.se" ++ self),
             address = address,
             area = area,
             municipality = municipality,
             rooms = (read rooms),
             squareArea = squareArea,
             totalRent = totalRent,
             hostName = hostName,
             objInfoList = objInfoList
           }

       objectInfoList = attr "data-apartment-type" $ "li" @: [hasClass "object-type"]



makeCsv :: [Apartment] -> String
makeCsv  apartments = (genCsvFile $ makeHeader : (map csvRow apartments))

makeHeader = ["self", "address", "area", "municipality", "rooms", "squareArea", "totalRent", "hostName", "objInfoList", "availableDate", "commuteDistance", "commuteFrequency", "commuteDuration"]

csvRow (Apartment link address area municipality rooms squareArea totalRent hostName obInfoList availableDate commuteDistance commuteFrequency commuteDuration) = [link, address, area, municipality, (show rooms), squareArea, totalRent, hostName, obInfoList, availableDate, commuteDistance, commuteFrequency, commuteDuration]

--saveApartment2 :: Apartment -> (Entity DBApartment)
--saveApartment2 a = insertEntity (toDbApartment a)

saveApartment :: Apartment -> HandlerFor App (Entity DBApartment)
saveApartment a = do
  runDB $ insertEntity (toDbApartment a)

toDbApartment :: Apartment -> DBApartment
toDbApartment (Apartment _self _address _area _municipality _rooms _squareArea _totalRent _hostName _objInfoList _availableDate _commuteDistance _commuteFrequency _commuteDuration) = DBApartment  _self _address _area _municipality _rooms _squareArea _totalRent _hostName _objInfoList _availableDate _commuteDistance _commuteFrequency _commuteDuration

fromDbApartment :: Entity DBApartment -> Apartment
fromDbApartment (Entity _ (DBApartment _self _address _area _municipality _rooms _squareArea _totalRent _hostName _objInfoList _availableDate _commuteDistance _commuteFrequency _commuteDuration)) = Apartment  _self _address _area _municipality _rooms _squareArea _totalRent _hostName _objInfoList _availableDate _commuteDistance _commuteFrequency _commuteDuration
