{-# LANGUAGE OverloadedStrings, UnicodeSyntax, DeriveGeneric #-}
module Slack
    ( say
    ) where
import Data.Maybe
import Data.Config
import Network.Wreq
import Control.Lens
import qualified Data.Text as T
import Data.Aeson
import GHC.Generics

import Settings


data Message = Message {
      text :: String
    } deriving (Generic, Show)

instance ToJSON Message


say msg config = do
  let url = slackUrl config
  let body = encode (Message msg)
  post (T.unpack url) body

