{-# LANGUAGE PackageImports #-}
import "housing-search" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
