{-# LANGUAGE Strict #-}

module Handler.Trigger where

import Import
import Apartment
import Handler.Apartment
import Lib (dataRetrievalJob)

postTriggerR :: Handler Value
postTriggerR = do

    app <- getYesod
    let settings = appSettings app
    liftIO $ dataRetrievalJob settings

    returnJson "OK"

