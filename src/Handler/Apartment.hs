module Handler.Apartment where

import Import
import Apartment (Apartment, apartmentDefault)

getApartmentR :: Handler Value
getApartmentR = do
    -- requireJsonBody will parse the request body into the appropriate type, or return a 400 status code if the request JSON is invalid.
    -- (The ToJSON and FromJSON instances are derived in the config/models file).
    let apartment = apartmentDefault
    returnJson [apartment]

