{-# LANGUAGE OverloadedStrings, UnicodeSyntax #-}
module Distance
    ( getDistances
    ) where

import Data.Config as C
import Network.Wreq
import Control.Lens
import qualified Data.Text as T
import Data.Text.Lazy.Encoding
import Data.Aeson
import GHC.Generics
import Text.HTML.Scalpel
import qualified Data.ByteString.Lazy as BL

import Apartment
import Data.Text.Lazy as L
import Debug.Trace
import System.Log.Logger

import Settings


type Coords = String

distanceLogger = "Distance"

getDistances :: AppSettings -> [Apartment] -> IO [Apartment]
getDistances config aa = do
  sequence $ Prelude.map (\x -> getDistance config x) aa

getDistance :: AppSettings -> Apartment -> IO Apartment
getDistance config a = do
  let srcAddress = address a
  let srcMunicipality = municipality a
  coords <- getCoords srcMunicipality srcAddress
  infoM distanceLogger $ "got coords: " ++ (show coords)
  (frequency, distance, duration) <- distanceQuery config coords
  return a { commuteFrequency = unpack frequency,
             commuteDistance = unpack distance,
             commuteDuration = unpack duration
  }

userAgent = "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 pSafari/537.36"

acceptLanguage = "en-US,en;q=0.9,sv;q=0.8"

distanceQuery :: AppSettings -> Coords -> IO (Text, Text, Text)
distanceQuery config coords = do
  let urlBase = distUrlBase config
  let urlSuffix = distUrlSuffix config
  let destination = destinationCoords config
  let url = (T.unpack urlBase) ++ coords ++ "/" ++ (T.unpack destination) ++ (T.unpack urlSuffix)
  infoM distanceLogger url
  r <- getWith opts $ url
  let html = unpack $ decodeUtf8 (r ^. responseBody)
  parseDistance $ pack html
  where opts = defaults & header "user-agent" .~ [userAgent]
                        & header "accept-language" .~ [acceptLanguage]

parseDistance html = do
  let a = splitOn "every" html
  if (Prelude.length a <= 1) then return ("", "", "") else splitMore a

splitMore a = do
  let b = splitOn "\"" $ a !! 1
  let frequency = stripText $ b !! 0
  let distance = stripText $ b !! 2
  let duration = stripText $ b !! 4
  return (frequency, distance, duration)

stripText s = (splitOn "\\" (strip s)) !! 0

replace a b s = Prelude.map (\c -> if c == a then b else c) (unpack s)

getCoords :: String -> String -> IO Coords
getCoords municipality address = do
    let url = "https://www.hitta.se/sök?vad=" ++ (makeHittaQuery municipality address)
    r <- get url
    infoM distanceLogger $ "Searching for coordinates, url: " ++ url
    let html = unpack $ decodeUtf8 (r ^. responseBody)
    case scrapeCoords html of
      Just val -> do
        if (Prelude.length val ) == 0 then return "" else return $ cleanCoords $ pack $ val !! 0
      Nothing -> return ""

makeHittaQuery :: String -> String -> String
makeHittaQuery municipality address = (urlEncode $ filteredAddress ++ "+" ++ municipality)
  where filteredAddress = unpack $ (splitOn "," (pack address)) !! 0

cleanCoords :: Text -> Coords
cleanCoords c = do
  let s = Prelude.last $ splitOn "/" c
  Prelude.map (\c -> if c == ':' then ',' else c) (unpack s)

urlEncode :: String -> String
urlEncode = Prelude.map $ \c -> if isSpace c  then '+' else c

isSpace :: Char -> Bool
isSpace c = c == ' ' || c == ' '

scrapeCoords :: String -> Maybe [String]
scrapeCoords html = do
  scrapeStringLike html coordList
  where
      coordList :: Scraper String [String]
      coordList = chroots ("li" @: [hasClass "result-row"]) coords

      coords :: Scraper String String
      coords = do
         coordsStr <- attr "href" $ "a" @: [hasClass "result-row-location__link"]
         return coordsStr

